# Sabooh

Sabooh is a Twitter bot inspired by [this script](https://github.com/MostafaAsadi/sabooh) by [Mostafa Asadi](https://github.com/MostafaAsadi) which publishes a Persian poem to its twitter account [_Sabooh](https://twitter.com/_Sabooh) every few hours.