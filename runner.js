const request = require("request-promise-native");
const twitter = require("twitter");

const client = new twitter({
  consumer_key: process.env.CONSUMER_KEY,
  consumer_secret: process.env.CONSUMER_SECRET,
  access_token_key: process.env.ACCESS_TOKEN_KEY,
  access_token_secret: process.env.ACCESS_TOKEN_SECRET
});

async function main() {
    var output = null;
    do {
        const beyt = JSON.parse(await request.get("http://v.ehsaan.me:5000"));

  output = `${beyt.m1}
${beyt.m2}

#${beyt.poet.replace(/ /g, "_")}`;
    }while(output.length > 280);
  client.post("statuses/update", { status: output }).catch(err => {
    console.error(err);
    process.exit(99);
  });
}

main();
